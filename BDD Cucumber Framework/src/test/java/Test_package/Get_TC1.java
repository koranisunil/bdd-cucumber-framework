package Test_package;

import java.io.File;
import java.io.IOException;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import API_common_methods.Common_method_handle_API;
import Endpoints.Get_endpoint;
import Parse_req.Get_Reqbody;
import Utility_common_methods.Handle_API_logs;
import Utility_common_methods.Handle_directory;

public class Get_TC1 extends Common_method_handle_API {

	static File log_dir;
	static String endpoint;
	static String responseBody;

	@BeforeTest

	public static void Test_Setup() {
		log_dir = Handle_directory.Create_log_directory("Get_TC1_logs");
		endpoint = Get_endpoint.Get_endpoint_TC1();

	}

	@Test
	public static void Get_executor() throws IOException {
		for (int i = 0; i < 5; i++) {
			int statuscode = get_statusCode(endpoint);
			System.out.println(statuscode);
			if (statuscode == 200) {

				responseBody = get_responseBody(endpoint);
				System.out.println(responseBody);

				Get_Reqbody.validator(responseBody);
				break;

			} else {
				System.out.println("Expected result not found retrying");
			}

		}

	}

	@AfterTest
	public static void Test_Teardown() throws IOException {
		String Test_Class_Name=Get_TC1.class.getName(); 
		Handle_API_logs.evidence_creator(log_dir, Test_Class_Name, endpoint, endpoint, responseBody);
	}
}
