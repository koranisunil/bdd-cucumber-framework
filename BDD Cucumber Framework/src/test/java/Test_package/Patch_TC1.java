package Test_package;

import java.io.File;
import java.io.IOException;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import API_common_methods.Common_method_handle_API;
import Endpoints.Patch_endpoint;
import Parse_req.Patch_Reqbody;
import Repository_Request.Patch_request_repository;
import Utility_common_methods.Handle_API_logs;
import Utility_common_methods.Handle_directory;

public class Patch_TC1 extends Common_method_handle_API {

	static File log_dir;
	static String requestBody;
	static String endpoint;
	static String responseBody;

	@BeforeTest

	public static void Test_Setup() throws IOException {
		log_dir = Handle_directory.Create_log_directory("Patch_TC1_logs");
		requestBody = Patch_request_repository.Patch_request_TC1();
		endpoint = Patch_endpoint.Patch_endpoint_TC1();

	}

	@Test
	public static void Patch_executor() throws IOException {
		for (int i = 0; i < 5; i++) {
			int statusCode = Patch_statusCode(requestBody, endpoint);
			System.out.println(statusCode);
			if (statusCode == 200) {

				responseBody = Patch_responseBody(requestBody, endpoint);
				System.out.println(responseBody);
				Patch_Reqbody.validator(requestBody, responseBody);

				break;

			} else {
				System.out.println("Expected Status code not found hence retrying");
			}
		}
	}

	@AfterTest
	public static void Test_Teardown() throws IOException {
		String Test_Class_Name=Patch_TC1.class.getName();
		Handle_API_logs.evidence_creator(log_dir, Test_Class_Name, endpoint, requestBody, responseBody);
	}
}
