package Parse_req;

import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;

public class Get_Reqbody {

	public static void validator(String responseBody) {

		int expected_id[] = { 7, 8, 9, 10, 11, 12 };
		String expected_first_name[] = { "Michael", "Lindsay", "Tobias", "Byron", "George", "Rachel" };
		String expected_last_name[] = { "Lawson", "Ferguson", "Funke", "Fields", "Edwards", "Howell" };
		String expected_email[] = { "michael.lawson@reqres.in", "lindsay.ferguson@reqres.in", "tobias.funke@reqres.in",
				"byron.fields@reqres.in", "george.edwards@reqres.in", "rachel.howell@reqres.in" };

		JSONObject array_res = new JSONObject(responseBody);
		JSONArray dataarray = array_res.getJSONArray("data");
		int count = dataarray.length();
		for (int i = 0; i < count; i++) {
			int res_id = dataarray.getJSONObject(i).getInt("id");
			String res_first_name = dataarray.getJSONObject(i).getString("first_name");
			String res_last_name = dataarray.getJSONObject(i).getString("last_name");
			String res_email = dataarray.getJSONObject(i).getString("email");
			int exp_id = expected_id[i];
			String exp_firstname = expected_first_name[i];
			String exp_lastname = expected_last_name[i];
			String exp_email = expected_email[i];
			Assert.assertEquals(res_id, exp_id);
			Assert.assertEquals(res_first_name, exp_firstname);
			Assert.assertEquals(res_last_name, exp_lastname);
			Assert.assertEquals(res_email, exp_email);

		}

	}

}
