package stepDefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

import static io.restassured.RestAssured.given;

import org.testng.Assert;
public class Post_API_data_driven_step_definition{
	String responseBody;
	String requestBody;
	int statusCode;
	
	@Given("Enter {string} & {string} in request Body")
	public void enter_in_request_body(String req_name, String req_job) {
		String BaseURI = "https://reqres.in/";
		RestAssured.baseURI = BaseURI;
		requestBody = "{\r\n" + "    \"name\": \""+req_name+"\",\r\n" + "    \""+req_job+"\": \"leader\"\r\n" + "}";
	    //throw new io.cucumber.java.PendingException();
	}
	

	@When("Send request with data")
	public void send_request_with_payload() {
		statusCode = given().header("Content-Type", "application/json").body(requestBody).when().post("/api/users")
				.then().extract().statusCode();
		responseBody = given().header("Content-Type", "application/json").body(requestBody).when()
				.post("/api/users").then().extract().response().asString();
		System.out.println(responseBody);
	    //throw new io.cucumber.java.PendingException();
	}
	@Then("Validate data driven status code")
	public void validate_status_code() {
		Assert.assertEquals(statusCode, 201);
		System.out.println(statusCode);
	    //throw new io.cucumber.java.PendingException();
	}
	@Then("Validate data driven responseBody parameter")
	public void validate_response_body_parameter() {
		JsonPath jsprequest = new JsonPath(requestBody);
		String req_name = jsprequest.getString("name");
		String req_job = jsprequest.getString("job");
		JsonPath jspresponse = new JsonPath(responseBody);
		String res_name=jspresponse.getString("name");
		String res_job=jspresponse.getString("job");
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		System.out.println("Response Body Validation Successfull");
	    //throw new io.cucumber.java.PendingException();
	}

}
