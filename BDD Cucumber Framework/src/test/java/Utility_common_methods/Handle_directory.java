package Utility_common_methods;

import java.io.File;

public class Handle_directory {
	public static File Create_log_directory(String log_dir) {
		// Fetch the current project Directory
		String project_dir = System.getProperty("user.dir");
		System.out.println("Current Project Directory path is : " +project_dir);
		File directory = new File(project_dir + "\\API_Logs\\" +log_dir);
		
		if(directory.exists()) {
			directory.delete();
			System.out.println(directory + ": deleted");
			directory.mkdir();
			System.out.println(directory.exists());
			System.out.println(directory + ": created");
		}
		else{
			directory.mkdir();
			System.out.println(directory.exists());
			System.out.println(directory + ": created");
		}
		return directory;
	}

}
