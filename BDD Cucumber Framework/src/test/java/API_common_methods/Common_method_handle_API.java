package API_common_methods;

import static io.restassured.RestAssured.given;

public class Common_method_handle_API {
	public static int Post_statusCode(String requestBody, String endpoint) {

		int statusCode = given().header("Content-Type", "application/json").body(requestBody).when().post(endpoint)
				.then().extract().statusCode();
		return statusCode;
	}

	public static String Post_responseBody(String requestBody, String endpoint) {

		String responseBody = given().header("Content-Type", "application/json").body(requestBody).when().post(endpoint)
				.then().extract().response().asString();
		return responseBody;

	}

//********PUT********
	public static int Put_statusCode(String requestBody, String endpoint) {

		int statuscode = given().header("Content-Type", "application/json").body(requestBody).when().put(endpoint)
				.then().extract().statusCode();
		return statuscode;
	}

	public static String Put_responseBody(String requestBody, String endpoint) {

		String responseBody = given().header("Content-Type", "application/json").body(requestBody).when().put(endpoint)
				.then().extract().response().asString();
		return responseBody;
	}
	// ********PATCH********

	public static int Patch_statusCode(String requestBody, String endpoint) {

		int statuscode = given().header("Content-Type", "application/json").body(requestBody).when().patch(endpoint)
				.then().extract().statusCode();
		return statuscode;
	}

	public static String Patch_responseBody(String requestBody, String endpoint) {

		String responseBody = given().header("Content-Type", "application/json").body(requestBody).when()
				.patch(endpoint).then().extract().response().asString();
		return responseBody;
	}
	// ********GET********

	public static int get_statusCode(String endpoint) {

		int statuscode = given().when().get(endpoint).then().extract().statusCode();
		return statuscode;
	}

	public static String get_responseBody(String endpoint) {

		String responseBody = given().when().get(endpoint).then().extract().response().asString();
		return responseBody;
	}
}
