Feature: Trigger Post API

Scenario Outline: Trigger the API request with valid request parameter
		  Given Enter "<Name>" & "<Job>" in request Body
		  When Send request with data
		  Then Validate data driven status code
		  And Validate data driven responseBody parameter
		  
Examples:
		  |Name |Job|
		  |Sunil|SrQA|
		  |Manisha|QA|
		  |Nikhil|JrQA|